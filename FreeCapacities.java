import java.util.ArrayList;
import java.util.Date;

public class FreeCapacities {
    public ArrayList<Capacity> GetFreeCapacities(ArrayList<Capacity> capacities,ArrayList<Utilization> utilizations) {
        ArrayList<Capacity> freeCapacities = new ArrayList<>();
        Capacity fullCapacity = new Capacity(capacities.get(0).getStart(),capacities.get(1).getEnd());
        Date start = fullCapacity.getStart();
        int j = 0;
        if(start.compareTo(utilizations.get(0).getStart()) > 0) {
            System.out.println("The machine can't work at this specific time: " + utilizations.get(0).getStart());
            return null;
        }
        if(start.compareTo(utilizations.get(0).getStart()) == 0) {
            start = utilizations.get(0).getEnd();
            j=1;
        }
        for(int i = 0;i<utilizations.size();i++) {
            if(start.compareTo(utilizations.get(j).getStart()) < 0) {
                freeCapacities.add(new Capacity(start, utilizations.get(j).getStart()));
                start = utilizations.get(j).getEnd();
                j++;
                if (j==utilizations.size()){
                    j--;
                }
            }
        }
        Date end = fullCapacity.getEnd();
        if(end.compareTo(utilizations.get(utilizations.size()-1).getEnd()) > 0) {
            freeCapacities.add(new Capacity(utilizations.get(utilizations.size()-1).getEnd(),end));
        }
        return freeCapacities;
    }
    public static void main(String[] args) {
        FreeCapacities free = new FreeCapacities();
        Utilization u = new Utilization();
        Capacity c = new Capacity();
        c.addCapacities("10/22/2018 6:00:00","10/22/2018 13:00:00");
        c.addCapacities("10/22/2018 14:00:00","10/22/2018 22:00:00");
        u.addUtilizations("10/22/2018 8:00:00","10/22/2018 9:00:00");
        u.addUtilizations("10/22/2018 11:00:00","10/22/2018 16:00:00");
        System.out.println("Capacities "+ free.GetFreeCapacities(c.capacities,u.utilizations));
    }
}
