import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
public class Utilization {
    private Date start;
    private Date end;

    public Utilization() {}

    public Utilization(Date start,Date end) {
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    protected ArrayList<Utilization> utilizations = new ArrayList<>();
    protected void addUtilizations(String startDateTime,String endDateTime) {
        SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy hh:mm:ss");
        Date startD = null;
        Date endD = null;
        try {
            startD = ft.parse(startDateTime);
            endD = ft.parse(endDateTime);
        } catch (ParseException e) {
            System.out.println("Cant parse using " + ft);
        }
        utilizations.add(new Utilization(startD,endD));
    }
}