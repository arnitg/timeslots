import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
public class Capacity {
    private Date start;
    private Date end;

    public Capacity(){}

    public Capacity(Date start,Date end) {
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    protected ArrayList<Capacity> capacities = new ArrayList<>();
    protected void addCapacities(String startDateTime,String endDateTime) {
        SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy hh:mm:ss");
        Date startD = null;
        Date endD = null;
        try {
            startD = ft.parse(startDateTime);
            endD = ft.parse(endDateTime);
        } catch (ParseException e) {
            System.out.println("Cant parse using " + ft);
        }
        capacities.add(new Capacity(startD,endD));
    }

    public String toString() {
        SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy HH:mm:ss");
        return  "\n\t{\n\t\tStart: " + ft.format(start) + "\n\t\tEnd: " + ft.format(end) +"\n\t}";
    }

}

